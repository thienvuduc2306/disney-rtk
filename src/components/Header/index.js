import React from "react";
import styled from "styled-components";
import data from "../../mock-data/header.json";

function Header() {
  return (
    <Nav>
      <Logo src="/images/logo.svg" />
      <NavMenu>
        {data.map((item) => (
          <a key={item.name} href={item.path}>
            <img src={item.img} alt={item.name} />
            <span>{item.name.toUpperCase()}</span>
          </a>
        ))}
      </NavMenu>
      <UserImg src="/images/avatar.jpg" alt="avatar" />
    </Nav>
  );
}

export default Header;
const Nav = styled.nav`
  height: 70px;
  background: #090b13;
  padding: 0 36px;
  display: flex;
  align-items: center;
  overflow-x: hidden;
`;
const Logo = styled.img`
  width: 80px;
`;
const NavMenu = styled.div`
  display: flex;
  flex: 1;
  margin-left: 25px;
  a {
    display: flex;
    align-items: center;
    padding: 0 12px;
    cursor: pointer;
    color: #fff;
    img {
      height: 20px;
    }
    span {
      font-size: 13px;
      letter-spacing: 1.42px;
      position: relative;

      &:after {
        content: "";
        height: 2px;
        background: #fff;
        position: absolute;
        left: 0;
        right: 0;
        bottom: -6px;
        transform: scaleX(0);
        transform-origin: left center;
        opacity: 0;
        transition: all 250ms ease-in;
      }
    }
    &:hover {
      span:after {
        transform: scaleX(1);
        opacity: 1;
      }
    }
  }
`;
const UserImg = styled.img`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
`;
