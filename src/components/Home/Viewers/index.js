import React from 'react'
import styled from 'styled-components'
import data from '../../../mock-data/viewers.json'

function Viewers() {
  return (
    <Container>
    {data.map(item => (
      <Wrap key={item.name}>
        <img src={item.img} alt={item.name} />
      </Wrap>
    ))}
    </Container>
  )
}

export default Viewers

const Container = styled.div`
margin-top: 30px;
display: grid;
grid-template-columns: repeat(5, minmax(0, 1fr));
grid-gap: 25px;
padding: 30px 0 26px;
`
const Wrap = styled.div`
border: 3px solid rgba(249, 249, 249, 0.1);
box-shadow: rgb(0 0 0 / 69%) 0px 26px 30px -10px, rgb(0 0 0 / 73%) 0px 16px 10px -10px;
border-radius: 10px;
img {
  width: 100%;
  height: 100%;
  object-fit: cover;
}
&:hover {
  transform: scale(1.05);
  cursor: pointer;
  border-color: rgba(249,249,249,0.8) ;
  transition: all 300ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
}
`