import React from "react";
import styled from "styled-components";
import data from "../../../mock-data/recomended.json";

function Movies() {
  return (
    <Container>
      <h4>Recomended for you</h4>
      <Content>
        {data.map((item) => (
          <Wrap key={item.name}>
            <img src={item.img} alt={item.name} />
          </Wrap>
        ))}
      </Content>
    </Container>
  );
}

export default Movies;

const Container = styled.div`
  padding: 20px 0;
  h4 {
    text-transform: uppercase;
  }
`;
const Content = styled.div`
  display: grid;
  grid-template-columns: repeat(4, minmax(0, 1fr));
  grid-gap: 25px;
`;
const Wrap = styled.div`
  border-radius: 10px;
  overflow: hidden;
  border: 3px solid rgba(249, 249, 249, 0.1);
  box-shadow: rgb(0 0 0 / 69%) 0px 26px 30px -10px, rgb(0 0 0 / 73%) 0px 16px 10px -10px;
  transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  &:hover {
    cursor: pointer;
    transform: scale(1.05);
    border-color: rgba(249, 249, 249, 0.8);
    overflow-y: hidden;
  }
`;
