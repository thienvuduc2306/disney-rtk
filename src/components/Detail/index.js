import React from "react";
import styled from "styled-components";

function Detail() {
  return (
    <Container>
      <Background>
        <img src="/images/detail-img.jpg" alt="detail" />
      </Background>
      <ImageTitle>
        <img src="/images/detail-img-title.png" alt="detail-title" />
      </ImageTitle>
      <Controls>
        <PlayButton>
          <img src="/images/play-icon-black.png" alt="button play" />
          <span>Play</span>
        </PlayButton>
        <TrailerButton>
          <img src="/images/play-icon-white.png" alt="button trailer" />
          <span>Trailer</span>
        </TrailerButton>
        <AddButton>
          <span>+</span>
        </AddButton>
        <GroupButton>
          <img src="/images/group-icon.png" alt="group-icon" />
        </GroupButton>
      </Controls>
      <SubTitle>2021 &#8226; 7m &#8226; Family, Fantasy, Kids, Animation</SubTitle>
      <Description>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi iste quasi excepturi nostrum, sapiente similique
        optio! Laudantium maxime quae omnis, aut fuga deleniti quo a sequi eligendi ad atque? Temporibus?
      </Description>
    </Container>
  );
}

export default Detail;

const Container = styled.div`
  min-height: calc(100vh - 70px);
  padding: calc(3.5vw + 5px);
  position: relative;
`;
const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;
const ImageTitle = styled.div`
  height: 30vh;
  min-height: 170px;
  width: 35vw;
  min-width: 200px;

  img {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`;
const Controls = styled.div`
  display: flex;
  align-items: center;
`;
const PlayButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 5px 24px;
  font-size: 15px;
  text-transform: uppercase;
  border-radius: 4px;
  border: none;
  background: rgb(249, 249, 249);
  letter-spacing: 1.8px;
  cursor: pointer;
  margin-right: 15px;
  &:hover {
    background: rgb(198, 198, 198);
  }
`;
const TrailerButton = styled(PlayButton)`
  background: rgba(0, 0, 0, 0.3);
  border: 2px solid rgb(249, 249, 249);
  color: #fff;
`;
const AddButton = styled.button`
  background: rgba(0, 0, 0, 0.3);
  border: 2px solid #fff;
  border-radius: 50%;
  width: 44px;
  height: 44px;
  cursor: pointer;
  margin-right: 15px;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    color: #fff;
    font-size: 30px;
  }
`;
const GroupButton = styled(AddButton)``;
const SubTitle = styled.div`
  font-size: 15px;
  margin-top: 20px;
`;
const Description = styled(SubTitle)`
  font-size: 20px;
  max-width: 760px;
  word-wrap: break-word;
`;
